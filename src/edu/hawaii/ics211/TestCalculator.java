/**
 * 
 */
package CalculatorLab.src.edu.hawaii.ics211;

/**
 * @author koday
 *
 */
public class TestCalculator {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
	int first=5;
	int second=2;
	int neg1=-8;
	int neg2=-10;
	
	Calculator TI83=new Calculator();
	
	System.out.println("Addition: "+first+"+"+second+ " should equal 7: "+TI83.add(first, second));
	System.out.println("Subtraction:  "+first+"-"+second+ " should equal 3: "+TI83.subtract(first, second));
	System.out.println("Multiplication: "+first+"*"+second+ "  should equal 10: "+TI83.multiply(first, second));
	System.out.println("Division:  "+first+"/"+second+ " should equal 2: "+ TI83.divide(first, second));
	System.out.println("Modulo: the modulo of "+first+" and "+second+" should equal 1: "+ TI83.modulo(first, second));
	System.out.println("Power:"+first+"^"+second+" should equal 25: "+TI83.pow(first, second));
	
	System.out.println("\n Addition: "+neg1+"+"+second+ " should equal -6: "+TI83.add(neg1, second));
	System.out.println("Subtraction:  "+neg1+"-"+second+ " should equal -10: "+TI83.subtract(neg1, second));
	System.out.println("Multiplication: "+neg1+"*"+second+ "  should equal -16: "+TI83.multiply(neg1, second));
	System.out.println("Division:  "+neg1+"/"+second+ " should equal 4: "+ TI83.divide(neg1, second));
	System.out.println("Modulo: the modulo of "+neg1+" and "+second+" should equal 0: "+ TI83.modulo(neg1, second));
	System.out.println("Power:"+neg1+"^"+second+" should equal 64: "+TI83.pow(neg1, second));
	
	System.out.println("\n Addition: "+first+"+"+neg2+ " should equal -5: "+TI83.add(first, neg2));
	System.out.println("Subtraction:  "+first+"-"+neg2+ " should equal 15: "+TI83.subtract(first, neg2));
	System.out.println("Multiplication: "+first+"*"+neg2+ "  should equal -50: "+TI83.multiply(first, neg2));
	System.out.println("Division:  "+first+"/"+neg2+ " should equal 0: "+ TI83.divide(first, neg2));
	System.out.println("Modulo: the modulo of "+first+" and "+neg2+" should equal 5: "+ TI83.modulo(first, neg2));
	System.out.println("Power:"+first+"^"+neg2+" should equal 1: "+TI83.pow(first, neg2));
	
	System.out.println("\n Addition: "+neg1+"+"+neg2+ " should equal -18: "+TI83.add(neg1, neg2));
	System.out.println("Subtraction:  "+neg1+"-"+neg2+ " should equal 2: "+TI83.subtract(neg1, neg2));
	System.out.println("Multiplication: "+neg1+"*"+neg2+ "  should equal 80: "+TI83.multiply(neg1, neg2));
	System.out.println("Division:  "+neg1+"/"+neg2+ " should equal 0: "+ TI83.divide(neg1, neg2));
	System.out.println("Modulo: the modulo of "+neg1+" and "+neg2+" should equal -8: "+ TI83.modulo(neg1, neg2));
	System.out.println("Power:"+neg1+"^"+neg2+" should equal 1: "+TI83.pow(neg1, neg2));
	}

}
